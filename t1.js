var input = require("readline-sync");
var word = input.question("Tarkista sana >>");

if (palindrome(word))
    console.log(word + " on palindromi.");
else
    console.log(word + " ei ole palindromi.");

function palindrome(word) {
    var reversed = "";
    for (var i = word.length - 1; i >= 0; i--)
        reversed += word[i];
    return word == reversed;
}