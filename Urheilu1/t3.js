class Henkilo
{
    constructor(etunimet, sukunimi, kutsumanimi, syntymavuosi)
    {
        this.etunimet = etunimet;
        this.sukunimi = sukunimi;
        this.kutsumanimi = kutsumanimi;
        this.syntymavuosi = syntymavuosi;
    }
}

class Urheilija extends Henkilo
{
    constructor(etunimet, sukunimi, kutsumanimi, syntymavuosi, kuvalinkki, omapaino, laji, saavutukset)
    {
        super(etunimet,sukunimi,kutsumanimi,syntymavuosi)
        this._kuvalinkki = kuvalinkki;
        this._omapaino = omapaino;
        this._laji = laji;
        this._saavutukset = saavutukset;
    }

    get kuvalinkki() { return this._kuvalinkki; }
    set kuvalinkki(value) { this._kuvalinkki = value; }

    get omapaino() { return this._omapaino; }
    set omapaino(value) { this._omapaino = value; }

    get laji() { return this._laji; }
    set laji(value) { this._laji = value; }

    get saavutukset() { return this._saavutukset; }
    set saavutukset(value) { this._saavutukset = value; }
}

var urheilija = new Urheilija("Slavoj", "Zizek", "Zizek", 1960, "https://kuva.jpg", 90, "hiihto", "maailmanmestaruus");
var urheilija2 = new Urheilija("Peter", "Jordanson", "Jorma", 2004, "https://kuva3.jpg", 70, "jääkiekko", "");
