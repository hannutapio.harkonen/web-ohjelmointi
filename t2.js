var input = require("readline-sync");

var phonebook = [];

function addContact(name, number) {
    phonebook.push({name:name, number:number});
}

function findNumber(contacts, name) {
    for (var i = 0; i < contacts.length; i++) {
        if (contacts[i].name == name)
            return contacts[i].number;
    }
    return -1;
}

addContact("aku", 123456789);
addContact("ankka", 987654321);

do {

    console.log("1 - Anna uusi kontakti");
    console.log("2 - Hae numero");
    console.log("3 - Lopeta");
    var choice = parseInt(input.question(">>"));

    switch (choice) {
        case 1:
            var name = input.question("Kontaktin nimi >>");
            var number = input.question("Kontaktin numero >>");

            addContact(name, number);
            break;
        case 2:
            name = input.question("Anna kontaktin nimi >>");

            var number = findNumber(phonebook, name);
            if (number != -1)
                console.log("Numero: " + number);
            else
                console.log("Numeroa ei ole.");
            break;
        case 3:
            console.log("Heippa sitten.");
            break;
        default:
            console.log("virhe");
    }

} while (choice != 3)