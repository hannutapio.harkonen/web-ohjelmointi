import React from "react";
import { Provider } from "./context";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Urheilijatiedot from "./components/Urheilijatiedot";
import Ylatunniste from "./components/Ylatunniste";
import LisaaUrheilijatieto from "./components/LisaaUrheilijatieto";
import MuokkaaUrheilijatieto from "./components/MuokkaaUrheilijatieto";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Tietoa from "./components/pages/Tietoa";

function App() {
  return (
    <Provider>
      <Router>
        <div className="App">
          <Ylatunniste />
          <div className="container">
            <Switch>
              <Route exact path="/" component={Urheilijatiedot} />
              <Route exact path="/tietoa" component={Tietoa} />
              <Route
                exact
                path="/urheilija/lisaa"
                component={LisaaUrheilijatieto}
              />
              <Route
                exact
                path="/urheilija/muokkaa/:id"
                component={MuokkaaUrheilijatieto}
              />
            </Switch>
          </div>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
