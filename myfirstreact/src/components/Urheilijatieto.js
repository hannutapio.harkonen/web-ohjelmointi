import React, { Component } from "react";
import PropTypes from "prop-types";
import { Consumer } from "../context";
import axios from "axios";
import { Link } from "react-router-dom";

export default class Urheilijatieto extends Component {
  state = { naytaUrheilijatieto: false };
  onDeleteClick = (id, dispatch) => {
    axios
      .delete(`http://localhost:3000/urheilijat/${id}`)
      .then((res) => dispatch({ type: "DELETE_URHEILIJATIETO", payload: id }));
  };
  onShowClick = (e) => {
    this.setState({ naytaUrheilijatieto: !this.state.naytaUrheilijatieto });
    console.log(this.state);
  };
  render() {
    const {
      id,
      nimi,
      syntymavuosi,
      paino,
      kuva,
      laji,
      saavutukset,
    } = this.props.urheilijatieto;
    const { naytaUrheilijatieto } = this.state;
    return (
      <Consumer>
        {(value) => {
          const { dispatch } = value;

          return (
            <div className="card card-body mb-3">
              <h4>
                {nimi}{" "}
                <button
                  className="button"
                  onClick={this.onShowClick.bind(this)}
                >
                  ...
                </button>
                <button
                  className="button_right"
                  onClick={this.onDeleteClick.bind(this, id, dispatch)}
                >
                  Poista
                </button>
                <Link id="muokkaaid" to={`urheilija/muokkaa/${id}`}>
                  <button className="button_right">Muokkaa</button>
                </Link>
              </h4>

              {naytaUrheilijatieto ? (
                <div class="d-flex flex-row">
                  <img
                    src={kuva}
                    alt="kuva urheilijasta"
                    width="200"
                    height="200"
                  />
                  <ul className="list-group">
                    <li className="list-group-item">
                      Syntymävuosi: {syntymavuosi}
                    </li>
                    <li className="list-group-item">Paino: {paino}</li>
                    <li className="list-group-item">Laji: {laji}</li>
                    <li className="list-group-item">
                      Saavutukset: {saavutukset}
                    </li>
                  </ul>
                </div>
              ) : null}
            </div>
          );
        }}
      </Consumer>
    );
  }
}

Urheilijatieto.propTypes = {
  yhteystieto: PropTypes.object.isRequired,
};
