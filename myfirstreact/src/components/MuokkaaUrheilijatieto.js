import React, { Component } from "react";
import { Consumer } from "../context";
import axios from "axios";

export default class MuokkaaUrheilijatieto extends Component {
  state = {
    nimi: "",
    syntymavuosi: 1999,
    paino: 0,
    kuva: "",
    laji: "",
    saavutukset: "",
  };

  onSubmit = (dispatch, e) => {
    e.preventDefault();

    const { nimi, syntymavuosi, paino, kuva, laji, saavutukset } = this.state;

    const paivitettyUrheilijatieto = {
      nimi: nimi,
      syntymavuosi: syntymavuosi,
      paino: paino,
      kuva: kuva,
      laji: laji,
      saavutukset: saavutukset,
    };

    const { id } = this.props.match.params;

    axios
      .put(`http://localhost:3000/urheilijat/${id}`, paivitettyUrheilijatieto)
      .then((res) => {
        dispatch({ type: "UPDATE_URHEILIJATIETO", payload: res.data });
        console.log(res);
      });

    this.setState({
      nimi: "",
      syntymavuosi: 1999,
      paino: 0,
      kuva: "",
      laji: "",
      saavutukset: "",
    });

    this.props.history.push("/");
  };

  onChange = (e) => this.setState({ [e.target.name]: e.target.value });

  componentDidMount() {
    const { id } = this.props.match.params;

    console.log(id);

    axios.get(`http://localhost:3000/urheilijat/${id}`).then((res) => {
      const tiedot = res.data;

      this.setState({
        nimi: tiedot.nimi,
        syntymavuosi: tiedot.syntymavuosi,
        paino: tiedot.paino,
        kuva: tiedot.kuva,
        laji: tiedot.laji,
        saavutukset: tiedot.saavutukset,
      });
    });
  }

  render() {
    const { nimi, syntymavuosi, paino, kuva, laji, saavutukset } = this.state;

    return (
      <Consumer>
        {(value) => {
          const { dispatch } = value;

          return (
            <div className="card mb-3">
              <div className="card-header">Muokkaa urheilijan tietoja</div>

              <div className="card-body">
                <form onSubmit={this.onSubmit.bind(this, dispatch)}>
                  <div className="form-group">
                    <label htmlFor="nimi">Nimi</label>

                    <input
                      type="text"
                      name="nimi"
                      className="form-control form-control-lg"
                      placeholder="Syötä nimi..."
                      value={nimi}
                      onChange={this.onChange}
                    />
                  </div>

                  <div className="form-group">
                    <label htmlFor="syntymavuosi">Syntymävuosi</label>

                    <input
                      type="text"
                      name="syntymavuosi"
                      className="form-control form-control-lg"
                      placeholder="Syötä syntymävuosi..."
                      value={syntymavuosi}
                      onChange={this.onChange}
                    />
                  </div>

                  <div className="form-group">
                    <label htmlFor="paino">Paino</label>

                    <input
                      type="text"
                      name="paino"
                      className="form-control form-control-lg"
                      placeholder="Syötä paino..."
                      value={paino}
                      onChange={this.onChange}
                    />
                  </div>

                  <div className="form-group">
                    <label htmlFor="kuva">Kuva</label>

                    <input
                      type="text"
                      name="kuva"
                      className="form-control form-control-lg"
                      placeholder="Syötä kuvalinkki..."
                      value={kuva}
                      onChange={this.onChange}
                    />
                  </div>

                  <div className="form-group">
                    <label htmlFor="laji">Laji</label>

                    <input
                      type="text"
                      name="laji"
                      className="form-control form-control-lg"
                      placeholder="Syötä laji..."
                      value={laji}
                      onChange={this.onChange}
                    />
                  </div>

                  <div className="form-group">
                    <label htmlFor="saavutukset">Saavutukset</label>

                    <input
                      type="text"
                      name="saavutukset"
                      className="form-control form-control-lg"
                      placeholder="Syötä saavutukset..."
                      value={saavutukset}
                      onChange={this.onChange}
                    />
                  </div>

                  <input
                    type="submit"
                    value="Muokkaa urheilijan tietoja"
                    className="btn btn-light btn-block"
                  />
                </form>
              </div>
            </div>
          );
        }}
      </Consumer>
    );
  }
}
