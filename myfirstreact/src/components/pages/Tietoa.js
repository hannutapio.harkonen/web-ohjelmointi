import React from "react";

export default function Tietoa() {
  return (
    <div>
      <h1 className="display-4">Tietoa Urheilijatietokannasta</h1>
      <p className="lead">
        Urheilijatietokanta -sovelluksen avulla voit hallita urheilijoiden tietoja
      </p>
      <p className="text-secondary">Versio 1.0.0</p>
    </div>
  );
}
