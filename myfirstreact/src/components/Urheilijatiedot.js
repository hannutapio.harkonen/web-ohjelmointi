import React, { Component } from "react";
import { Consumer } from "../context";
import Urheilijatieto from "./Urheilijatieto";

export default class Urheilijatiedot extends Component {
  state = {
    urheilijatiedot: "",
  };

  render() {
    return (
      <Consumer>
        {(value) => {
          const { urheilijatiedot } = value;
          return (
            <div>
              <h1 className="display-4 mb-2">
                <span className="text-danger">Urheilijatietokanta</span>
              </h1>
              {urheilijatiedot.map((urheilijatieto) => (
                <Urheilijatieto key={urheilijatieto.id} urheilijatieto={urheilijatieto} />
              ))}
            </div>
          );
        }}
      </Consumer>
    );
  }
}
