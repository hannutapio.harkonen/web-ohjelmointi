import React, { Component } from "react";
import axios from "axios";

const Context = React.createContext();

const reducer = (state, action) => {
  switch (action.type) {
    case "DELETE_URHEILIJATIETO":
      return {
        ...state,

        urheilijatiedot: state.urheilijatiedot.filter(
          (urheilijatieto) => urheilijatieto.id !== action.payload
        ),
      };
    case "ADD_URHEILIJATIETO":
      return {
        ...state,

        urheilijatiedot: [...state.urheilijatiedot, action.payload],
      };
    case "UPDATE_URHEILIJATIETO":
      return {
        ...state,

        urheilijatiedot: state.urheilijatiedot.map((urheilijatieto) =>
          urheilijatieto.id === action.payload.id
            ? (urheilijatieto = action.payload)
            : urheilijatieto
        ),
      };
    default:
      return state;
  }
};

export class Provider extends Component {
  state = {
    urheilijatiedot: [],
    dispatch: (action) => this.setState((state) => reducer(state, action)),
  };

  componentDidMount() {
    axios
      .get("http://localhost:3000/urheilijat")
      .then((res) => this.setState({ urheilijatiedot: res.data }));
  }

  render() {
    return (
      <Context.Provider value={this.state}>
        {this.props.children}
      </Context.Provider>
    );
  }
}

export const Consumer = Context.Consumer;
