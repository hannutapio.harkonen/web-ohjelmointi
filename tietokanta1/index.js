const mysql = require("mysql");
const con = mysql.createConnection({
  host: "localhost",
  user: "HT",
  password: "manaatti",
  database: "urheilutietokanta",
});
con.connect((err) => {
  if (err) {
    console.log("Error connecting to Db");
    return;
  }
  console.log("Connection established");
});

const express = require("express");
const bodyParser = require("body-parser");
const app = express().use(bodyParser.json());

// Add headers
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});


// GET all users
app.get("/urheilijat", (req, res) => {
  con.query("SELECT * FROM urheilijat", (err, urheilijat) => {
    if (err) throw err;
    res.json(urheilijat);
  });
});
// GET a user
app.get("/urheilijat/:id", (req, res) => {
  const id = Number(req.params.id);
  con.query("SELECT * FROM urheilijat where id=?", id, (err, urheilijat) => {
    if (err) throw err;
    res.json(urheilijat ? urheilijat[0] : { message: "Not found" });
  });
});
// ADD a user
app.post("/urheilijat", (req, res) => {
  const urheilija = req.body;
  con.query("INSERT INTO urheilijat SET ?", urheilija, (err, result) => {
    if (err) throw err;
    console.log("Last insert ID:", result.insertId);
    res.json({ id: result.insertId, nimi: urheilija.nimi, syntymavuosi: urheilija.syntymavuosi, paino: urheilija.paino, kuva: urheilija.kuva, laji: urheilija.laji, saavutukset: urheilija.saavutukset });
  });
});
// UPDATE a user
app.put("/urheilijat/:id", (req, res) => {
  const id = Number(req.params.id);
  const urheilija = req.body;
  con.query(
    "UPDATE urheilijat SET ? Where ID = ?",
    [urheilija, id],
    (err, result) => {
      if (err) throw err;
      console.log(`Changed ${result.changedRows} row(s)`);
      res.json({ id: id, nimi: urheilija.nimi, syntymavuosi: urheilija.syntymavuosi, paino: urheilija.paino, kuva: urheilija.kuva, laji: urheilija.laji, saavutukset: urheilija.saavutukset });
    }
  );
});
// DELETE a user
app.delete("/urheilijat/:id", (req, res) => {
  const id = Number(req.params.id);
  con.query("DELETE FROM urheilijat WHERE id = ?", id, (err, result) => {
    if (err) throw err;
    console.log(`Deleted ${result.affectedRows} row(s)`);
    res.json(result);
  });
});
app.listen(3000, () => {
  console.log("Server listening at port 3000");
});

// con.query("SELECT * FROM henkilot", (err, rows) => {
//   if (err) throw err;
//   console.log("Data received from Db:");
//   console.log(rows);
// });

// const henkilo = { nimi: "Ankka Roope", puhelin: "050-1231232" };
// con.query("INSERT INTO henkilot SET ?", henkilo, (err, res) => {
//   if (err) throw err;
//   console.log("Last insert ID:", res.insertId);
// });

// con.query(
//   "UPDATE henkilot SET puhelin = ? Where ID = ?",
//   ["044-6544655", 3],
//   (err, result) => {
//     if (err) throw err;
//     console.log(`Changed ${result.changedRows} row(s)`);
//   }
// );

// con.query("DELETE FROM henkilot WHERE id = ?", [6], (err, result) => {
//   if (err) throw err;
//   console.log(`Deleted ${result.affectedRows} row(s)`);
// });

// con.query("CALL sp_get_henkilot()", function (err, rows) {
//   if (err) throw err;

//   rows[0].forEach( (row) => {
//     console.log(`${row.nimi},  puhelin: ${row.puhelin}`);
//   });
//   console.log(rows);
// });

// con.query("CALL sp_get_henkilon_tiedot(1)", (err, rows) => {
//   if (err) throw err;

//   console.log("Data received from Db:\n");
//   console.log(rows[0]);
// });

// con.query(
//     "SET @henkilo_id = 0; CALL sp_insert_henkilo(@henkilo_id, 'Matti Miettinen', '044-5431232'); SELECT @henkilo_id",
//     (err, rows) => {
//       if (err) throw err;

//       console.log('Data received from Db:\n');
//       console.log(rows);
//     }
//   );
// const userSubmittedVariable =
//   "1"; /*ettÃ¤ kukaan ei voi syÃ¶ttÃ¤Ã¤ tÃ¤tÃ¤:
// const userSubmittedVariable = '1; DROP TABLE henkilot';*/

// con.query(
//   `SELECT * FROM henkilot WHERE id = ${mysql.escape(userSubmittedVariable)}`,
//   (err, rows) => {
//     if (err) throw err;
//     console.log(rows);
//   }
// );

// con.end((err) => {
//   // The connection is terminated gracefully
//   // Ensures all remaining queries are executed
//   // Then sends a quit packet to the MySQL server.
// });
